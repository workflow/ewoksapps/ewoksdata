# CHANGELOG.md

## Unreleased

## 1.0.0

Changes:

- Drop Python 3.6 and 3.7
- Explicit HDF5 flushing for `_DatasetWriterBase` derived classes.
- `ewoksdata.test.data.bliss_scans`: `save_bliss_scan` can now save positioners and a scan title.

Dependency fixes:

- `blissdata` uses a private method from `silx` which is removed since `2.2`.

## 0.6.0

New Features:

- Iterate Bliss data from memory within a slice
- Support `blissdata` 2.0

## 0.5.1

Bug fixes:

- `ContextIterator` was not a proper generator API

## 0.5.0

- `iter_bliss_data` and `iter_bliss_scan_data` are context iterators: context managers that yield an iterator
  but can also be used as a normal iterator, similar to python's `open` which can be used as a context manager
  that yields a file object or as a normal file object.

## 0.4.4

Bug fixes:

- Support Python 3.12

## 0.4.3

Bug fixes:

- Support pip 24.1

## 0.4.2

Bug fixes:

- `iter_bliss_scan_data_from_memory` was yielding lima data wrong

## 0.4.1

Bug fixes:

- The blissdata<1 dependency does not have redis

## 0.4.0

New Features:

- Support Bliss 2.0

## 0.3.0

Breaking changes:

- Replaced the `bliss` extra install option with `online`

Bug fixes:

- Check for empty-directory when calling os.makedirs

## 0.2.7

New Features:

- `StackDatasetWriter`: write HDF5 dataset with one extra dimension than `DatasetWriter`

Bug fixes:

- `iter_bliss_scan_data_from_memory` capture lima exceptions

## 0.2.6

Changes:

- `get_data` support non-HDF5 URL's without scheme

## 0.2.5

Changes:

- `iter_bliss_scan_data_from_memory` retry getting the lima proxy

Bug fixes:

- `create_nexus_group` bug when no directory is provided
- `iter_bliss_scan_data_from_memory` check lima acquisition number

## 0.2.4

Bug fixes:

- `DatasetWriter` time based flushing fix alignment

## 0.2.3

Changes:

- retry getting the Redis node when getting data from memory
- `DatasetWriter` optional time-based flushing

Bug fixes:

- fix `blissdata` bug

## 0.2.2

Changes:

- add `h5exists`
- `DatasetWriter` fill value is NaN
- `DatasetWriter` support dataset attributes

Bug fixes:

- `DatasetWriter` for small datasets without chunking failed

## 0.2.1

Bug fixes:

- missing `blissdata` dependency

## 0.2.0

Changes:

- use `blissdata` to iterate over scan data

## 0.1.1

New Features:

- `DatasetWriter`: works as a context manager

## 0.1.0

New Features:

- `iter_bliss_data`: iterator for online Bliss data
- `get_data`: get online Bliss data
- `DatasetWriter`: write HDF5 dataset with chunk guessing and alignment
- `last_lima_image`: last image of a running scan
