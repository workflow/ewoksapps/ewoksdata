ewoksdata |version|
===================

*ewoksdata* provides utilities for data access by `ewoks <https://ewoks.readthedocs.io/>`_ workflows.

*ewoksdata* has been developed by the `Software group <http://www.esrf.fr/Instrumentation/software>`_ of the `European Synchrotron <https://www.esrf.fr/>`_.

Getting started
---------------

.. code:: bash
    
    pip install ewoksdata


Iterate over the data from one *Bliss* scan during or after acquisition
-----------------------------------------------------------------------

.. code:: python

    from ewoksdata.data.bliss import iter_bliss_scan_data

    filename = "/data/id31/inhouse/id312210/id31/20221001/testpyfai/testpyfai_0001/testpyfai_0001.h5"
    scan_nr = 38

    with iter_bliss_scan_data(
        filename, scan_nr, lima_names=["p3"], counter_names=["mondio"], retry_timeout=10
    ) as bliss_scan_data_iterator:
        for data in bliss_scan_data_iterator:
            print(data["p3"]) # Contains lima data
            print(data["mondio"]) # Contains counter data

.. warning::

    To ensure you get as many data points as scan points, make sure to specify at least one counter.

    Also, to ensure that the file is closed after iterating on the scan data, make sure to use the iterator as a context manager as shown.


Iterate from a certain index of a *Bliss* scan during or after acquisition
--------------------------------------------------------------------------

.. code:: python

    from ewoksdata.data.bliss import iter_bliss_data

    filename = "/data/id31/inhouse/id312210/id31/20221001/testpyfai/testpyfai_0001/testpyfai_0001.h5"
    scan_nr = 38
    start_index = 5

    with iter_bliss_data(
        filename, scan_nr, lima_names=["p3"], counter_names=["mondio"], retry_timeout=10, start_index=start_index
    ) as bliss_data_iterator:
        for index, data in bliss_data_iterator:
            print(data["p3"]) # Contains lima data
            print(data["mondio"]) # Contains counter data

.. warning::

    To ensure you get as many data points as scan points, make sure to specify at least one counter.

    Also, to ensure that the file is closed after iterating on the scan data, make sure to use the iterator as a context manager as shown.

.. toctree::
    :hidden:

    api
