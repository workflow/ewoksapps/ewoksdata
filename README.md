# ewoksdata

Data access utilities for [ewoks](https://ewoks.readthedocs.io/) workflows.

## Documentation

https://ewoksdata.readthedocs.io
